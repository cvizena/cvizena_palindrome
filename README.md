# Palindrome detector

`cvizena_palindrome` is a sample Ruby gem created by Claudia Vizena.

## Installation

To install `cvizena_palindrome`, add this line to your application's `Gemfile`:

```
gem 'cvizena_palindrome'
```

Then install as follows:

```
$ bundle install
```

Or install it directly using `gem`:

```
$ gem install cvizena_palindrome
```

## Usage

`cvizena_palindrome` adds a `palindrome?` method to the `String` class, and can be used as follows:

```
$ irb
>> require 'cvizena_palindrome'
>> "honey badger".palindrome?
=> false
>> "deified".palindrome?
=> true
>> "Able was I, ere I saw Elba.".palindrome?
=> true
>> phrase = "Madam, I'm Adam."
>> phrase.palindrome?
=> true
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
